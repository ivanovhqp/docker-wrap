# Installation

Must be installed:

* docker
* docker-compose

First, clone this repository:

```bash
$ git clone git@bitbucket.org:ivanovhqp/standard.git
$ cd docker-frame
$ ./unrolling
```

# Cron

Execute ./setcrone or:

```bash
docker exec dockerframe_php chown root:crontab /var/spool/cron/crontabs/root
docker exec dockerframe_php chmod 600 /var/spool/cron/crontabs/root
docker exec dockerframe_php cron /var/spool/cron/crontabs/root
docker exec dockerframe_php /etc/init.d/cron restart
```

# How it works?

Here are the `docker-compose` built images:

* `db`: This is the percona MySQL database container (can be changed to postgresql or whatever in `docker-compose.yml` file),
* `php`: This is the sibnet-php-fpm-5.6 container including the application volume mounted on,
* `nginx`: This is the sibnet-nginx webserver container in which php volumes are mounted too.

# Read logs

You can access Nginx and php-fpm application logs in the following directories on your host machine:

* `logs/nginx`
* `logs/php-fpm`